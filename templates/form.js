export default (todo) => {
  let [isOne, isTwo, isThree] = [false, false, false]

  switch (todo.priority) {
    case '1':
      isOne = 'selected'
      break
    case '2':
      isTwo = 'selected'
      break
    case '3':
      isThree = 'selected'
      break
  }

  return `
    <div class="modal-body">
      <form name="todoForm" id="todoForm" data-uid="${todo.uid}">
        <div class="form-group">
          <label for="description">Description</label>
          <input type="text" class="form-control" name="description" value="${todo.description}"/>
        </div>
        <div class="form-group">
          <label for="priority">Priority</label>
          <select class="form-control" name="priority">
            <option value="1" ${isOne}>High</option>
            <option value="2" ${isTwo}>Medium</option>
            <option value="3" ${isThree}>Low</option>
          </select>
        </div> 
        <label for="targetDate">Target Date</label>
        <div class="input-group date">
          <input type="text" class="form-control datepicker" name="targetDate" value="${todo.targetDate}"/>
          <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input class="btn btn-primary" type="submit"/>
        </div>
      </form>
    </div>
  `
}
