export default (filterDone, sort) => {
  return `
    <nav class="navbar navbar-default navbar-fixed-top">
      <button type="button" id="addTodo" class="btn btn-success btn-lg" data-toggle="modal" data-target="#todoModal">+</button>
      <select class="selectpicker sort">
        <option ${sort === 'none' ? 'checked': ''} value="none">Sort by</option>
        <option ${sort === 'targetDate' ? 'checked': ''} value="targetDate">Target Date</option>
        <option ${sort === 'priority' ? 'checked': ''} value="priority">Priority</option>
      </select>
      <span class="filter-all ${filterDone ? '' : 'active'}">All</span>
      <span class="filter-done ${filterDone ? 'active' : ''}">Active</span>
    </nav>
  `
}
