import firebase from '../firebase'
import todosChange from '../handlers/todosChange'

export default (filterDone, sortBy) => {
    firebase.database().ref('todos/').off('value')
    firebase.database().ref('todos/').on('value', todosChange(filterDone, sortBy))

    if (!filterDone) {
      $('.filter-all').css('outline', '1px solid lightblue')
      $('.filter-done').css('outline', '')
    } else {
      $('.filter-all').css('outline', 'none')
      $('.filter-done').css('outline', '1px solid lightblue')
    }
}
