import firebase from '../firebase'
import todosChange from '../handlers/todosChange'

export default (e, filterDone, sortBy) => {
  firebase.database().ref('todos/').off('value')
  firebase.database().ref('todos/').on('value', todosChange(filterDone, sortBy))
}
