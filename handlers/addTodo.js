import firebase from '../firebase'
import form from '../templates/form'
import modal from '../templates/modal'

const originalDraft = {
  description: '',
  priority: 1,
  targetDate: '',
  done: false
}

export default () => {
  let todoForm = form(Object.assign({}, originalDraft))

  $('#modal-container').html(modal(todoForm))
  $('.datepicker').datepicker()
  $('#todoForm').submit(function (e) {
    e.preventDefault()
    const formVals = $(this).serializeArray()

    const todo = {}
    formVals.forEach((val) => {
      todo[val.name] = val.value
    })

    todo.priority = parseInt(todo.priority)
    todo.done = false

    $('#todoModal').modal('hide')
    firebase.database().ref('todos/').push(todo)
  })
}
