import firebase from './firebase'
import addTodo from './handlers/addTodo'
import todosChange from './handlers/todosChange'
import filter from './handlers/filter'
import nav from './templates/nav'
import sort from './handlers/sort'

$('#nav-container').html(nav(filterDone, sortBy))
$('#addTodo').click(addTodo)

firebase.database().ref('todos/').on('value', todosChange())

let filterDone = false
let sortBy = 'none'

$('.filter-all').click(() => {
  if (filterDone) {
    filterDone = false
    filter(filterDone, sortBy)
  }
})

$('.filter-done').click(() => {
  if (!filterDone) {
    filterDone = true
    filter(filterDone, sortBy)
  }
})

$('.sort').change((e) => {
  if (sortBy !== e.target.value) {
    sortBy = e.target.value
    sort(e, filterDone, sortBy)
  }
})
