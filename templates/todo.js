export default (todos) => {
  return todos.reduce((prev, todo) => {
    const checked = todo.done ? 'checked' : ''

    let className
    switch (todo.priority) {
      case 1:
        className = 'danger'
        break
      case 2:
        className = 'warning'
        break
      default:
        className = ''
    }

    return prev + `
      <tr class="${className}">
        <td>
          <input data-uid="${todo.uid}" class="done" type="checkbox" ${checked}/>
        </td>
        <td class="todo"
          data-target="#todoModal"
          data-toggle="modal"
          data-uid="${todo.uid}">
          ${todo.description}
        </td>
        <td>${todo.targetDate}</td>
      </tr>
    `
  }, ``)
}
