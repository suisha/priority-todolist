import firebase from 'firebase'

// http://stackoverflow.com/a/37484053/886596
var config = {
  apiKey: "AIzaSyAenkuAZde3B1yyZdbFuzSjJziN8mK5JNo",
  authDomain: "quickstart-99338.firebaseapp.com",
  databaseURL: "https://quickstart-99338.firebaseio.com",
  storageBucket: "quickstart-99338.appspot.com",
};

firebase.initializeApp(config);

export default firebase
