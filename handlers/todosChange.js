import firebase from '../firebase'
import form from '../templates/form'
import modal from '../templates/modal'
import todoTable from '../templates/todos'

export default (filterDone, sortBy) => {
  return (snapshot) => {
    let todos = []
    const todoMap = snapshot.val()
    for (var key in todoMap) {
      let todo = todoMap[key]
      todo.uid = key
      todos.push(todo)
    }

    if (filterDone) {
      todos = todos.filter((todo) => {
        return !todo.done
      })
    }

    if (sortBy && sortBy === 'targetDate') {
      todos.sort((a, b) => {
        return new Date(a.targetDate) - new Date(b.targetDate)
      })
    } else if (sortBy) {
      todos.sort((a, b) => {
        return a[sortBy] - b[sortBy]
      })
    }

    $('#container').html(todoTable(todos))

    $('.done').change(function (e) {
      const uid = e.target.getAttribute('data-uid') 
      firebase.database().ref('todos/' + uid).update({
        done: $(this).is(':checked')
      })
    })

    $('.todo').click((e) => {
      const uid = e.target.getAttribute('data-uid')
      let todo = todoMap[uid]
      let todoForm = form(Object.assign({uid: uid}, todo))

      $('#modal-container').html(modal(todoForm))
      $('.datepicker').datepicker()

      $('#todoForm').submit(function (e) {
        e.preventDefault()
        const uid = e.target.getAttribute('data-uid')
        const formVals = $(this).serializeArray()

        const todo = {}
        formVals.forEach((val) => {
          todo[val.name] = val.value
        })

        todo.priority = parseInt(todo.priority)

        $('#todoModal').modal('hide')
        firebase.database().ref('todos/' + uid).update(todo)
      })
    })
  }
}
