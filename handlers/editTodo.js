import firebase from '../firebase'
import form from '../templates/form'
import modal from '../templates/modal'

export default (todoMap) => {
  return (e) => {
    const uid = e.target.getAttribute('data-uid')
    let todo = todoMap[uid]
    let todoForm = form(Object.assign({uid: uid}, todo))

    $('#modal-container').html(modal(todoForm))
    $('.datepicker').datepicker()

    $('#todoForm').submit(function (e) {
      e.preventDefault()
      const uid = e.target.getAttribute('data-uid')
      const formVals = $(this).serializeArray()

      const todo = {}
      formVals.forEach((val) => {
        todo[val.name] = val.value
      })

      firebase.database().ref('todos/' + uid).update(todo)
      $('#todoModal').modal('hide')
    })
  }
}
