import todoRow from './todo'

export default (todos) => {
  const rows = todoRow(todos)

  return `
    <table class="table table-hover table-bordered" data-toggle="table">
      <thead>
        <th class="complete"></th>
        <th class="todo">Todo</th>
        <th class="target-date">Target Date</th>
      </thead>
      <tbody id="todoBody">
        ${rows}
      </tbody>
    </table>
  `
}
